package li.longi.USBTransferThread;

import java.nio.ByteBuffer;

import org.usb4java.Transfer;

public final class RestrictedTransfer {
	private final Transfer transfer;

	public RestrictedTransfer(final Transfer transfer) {
		this.transfer = transfer;
	}

	/**
	 * Returns the address of the endpoint where this transfer will be sent.
	 *
	 * @return The endpoint address.
	 */
	public byte endpoint() {
		return transfer.endpoint();
	}

	/**
	 * Returns the type of the endpoint.
	 *
	 * @return The endpoint type.
	 */
	public byte type() {
		return transfer.type();
	}

	/**
	 * Returns the timeout for this transfer in milliseconds. A value of 0
	 * indicates no timeout.
	 *
	 * @return The timeout.
	 */
	public long timeout() {
		return transfer.timeout();
	}

	/**
	 * Returns the status of the transfer. Read-only, and only for use within
	 * transfer callback function.
	 *
	 * @return The transfer status.
	 */
	public int status() {
		return transfer.status();
	}

	/**
	 * Returns the length of the data buffer.
	 *
	 * @return The data buffer length.
	 */
	public int length() {
		return transfer.length();
	}

	/**
	 * Returns the actual length of data that was transferred. Read-only, and
	 * only for use within transfer callback function. Not valid for isochronous
	 * endpoint transfers.
	 *
	 * @return The actual length of the transferred data.
	 */
	public int actualLength() {
		return transfer.actualLength();
	}

	/**
	 * Returns the current user data object.
	 *
	 * @return The current user data object.
	 */
	public Object userData() {
		return transfer.userData();
	}

	/**
	 * Sets the user data object, representing user context data to pass to
	 * the callback function and that can be accessed from there.
	 *
	 * @param userData
	 *            The user data object to set.
	 */
	public void setUserData(final Object userData) {
		transfer.setUserData(userData);
	}

	/**
	 * Returns the data buffer.
	 *
	 * @return The data buffer.
	 */
	public ByteBuffer buffer() {
		return transfer.buffer();
	}
}
