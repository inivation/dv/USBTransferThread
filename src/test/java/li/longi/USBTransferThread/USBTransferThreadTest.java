package li.longi.USBTransferThread;

import java.nio.IntBuffer;
import java.util.concurrent.atomic.AtomicLong;

import li.longi.USBTransferThread.RestrictedTransfer;
import li.longi.USBTransferThread.RestrictedTransferCallback;
import li.longi.USBTransferThread.USBTransferThread;

import org.usb4java.BufferUtils;
import org.usb4java.Device;
import org.usb4java.DeviceDescriptor;
import org.usb4java.DeviceHandle;
import org.usb4java.DeviceList;
import org.usb4java.LibUsb;

public final class USBTransferThreadTest {
	// Length of time the test shall run
	private static final int TEST_TIME = 5;
	// Test changing the buffer numbers and sizes (!!! breaks transfer-rate calculation !!!)
	private static final boolean VARY_BUFFERS = false;

	public static void main(final String[] args) {
		final AtomicLong tCompleted = new AtomicLong();
		final AtomicLong tFailed = new AtomicLong();

		final class USBPerfTest implements RestrictedTransferCallback {
			@Override
			public void prepareTransfer(final RestrictedTransfer transfer) {
				// Nothing to do here.
			}

			@Override
			public void processTransfer(final RestrictedTransfer transfer) {
				if ((transfer.status() == LibUsb.TRANSFER_COMPLETED) && (transfer.actualLength() == transfer.length())) {
					tCompleted.incrementAndGet();
				}
				else {
					tFailed.incrementAndGet();
				}
			}
		}

		LibUsb.init(null);

		final DeviceList l = new DeviceList();
		LibUsb.getDeviceList(null, l);

		// FX3 SRC_SINK performance testing device.
		final short VID = (short) 0x152A;
		final short PID = (short) 0x841F;

		final DeviceHandle fx3 = new DeviceHandle();
		final DeviceDescriptor ddesc = new DeviceDescriptor();

		for (final Device d : l) {
			LibUsb.getDeviceDescriptor(d, ddesc);

			if ((ddesc.idVendor() == VID) && (ddesc.idProduct() == PID)) {
				if (LibUsb.open(d, fx3) == LibUsb.SUCCESS) {
					System.out.println("Found FX3 SRC_SINK and opened successfully.");

					break;
				}
			}
		}

		LibUsb.freeDeviceList(l, true);

		if (fx3.getPointer() != 0) {
			// Initialize device.
			final IntBuffer activeConfig = BufferUtils.allocateIntBuffer();
			LibUsb.getConfiguration(fx3, activeConfig);

			if (activeConfig.get() != 1) {
				LibUsb.setConfiguration(fx3, 1);
			}

			LibUsb.claimInterface(fx3, 0);

			// Do transfer test here.
			final USBTransferThread usbt = new USBTransferThread(fx3, (byte) (LibUsb.ENDPOINT_IN | 0x02),
				LibUsb.TRANSFER_TYPE_BULK, new USBPerfTest(), 8, 8 * 1024);
			usbt.start();

			System.out.println("Started benchmark ...");

			try {
				int countdown = USBTransferThreadTest.TEST_TIME;
				long lastCompleted = tCompleted.get();

				while ((countdown != 0) && usbt.isAlive()) {
					Thread.sleep(1000);
					final long lCompleted = tCompleted.get();
					final long lFailed = tFailed.get();

					System.out.println(String.format("1s elapsed, counters: tCompleted %d, tFailed %d", lCompleted,
						lFailed));
					System.out.println(String.format("Moved %d buffers (number: %d, size: %d), averaging %d bytes/sec",
						lCompleted - lastCompleted, usbt.getBufferNumber(), usbt.getBufferSize(),
						(lCompleted - lastCompleted) * usbt.getBufferSize()));

					countdown--;
					lastCompleted = lCompleted;

					if (USBTransferThreadTest.VARY_BUFFERS) {
						usbt.setBufferNumber((int) (usbt.getBufferNumber() * 1.5));
						usbt.setBufferSize(usbt.getBufferSize() * 2);
						System.out.println(String.format("Increasing buffers number and size to %d x %d bytes",
							usbt.getBufferNumber(), usbt.getBufferSize()));
					}
				}
			}
			catch (final InterruptedException e) {
				e.printStackTrace();
			}

			// Stop test by interrupting thread, it will then cleanup.
			usbt.interrupt();

			try {
				usbt.join();
			}
			catch (final InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("Completed benchmark ...");

			LibUsb.releaseInterface(fx3, 0);
			LibUsb.close(fx3);

			final long lCompleted = tCompleted.get();
			final long lFailed = tFailed.get();

			System.out.println(String.format("Counter's status: tCompleted %d, tFailed %d", lCompleted, lFailed));
			System.out.println(String.format("Moved %d buffers (number: %d, size: %d) for a total of %d bytes",
				lCompleted, usbt.getBufferNumber(), usbt.getBufferSize(), (lCompleted * usbt.getBufferSize())));
			System.out.println(String.format("Resulting speed: ~ %d bytes/sec", (lCompleted * usbt.getBufferSize())
				/ USBTransferThreadTest.TEST_TIME));
		}

		LibUsb.exit(null);
	}
}
